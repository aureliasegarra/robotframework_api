*** Settings ***
Documentation   Robot Framework Demo

Library     Collections
Library     String
Library     JSONLibrary
Library     RequestsLibrary

*** Variables ***

${base_url}         https://reqres.in
${page_path}        $.page
${id_path}          $.id

*** Test Cases ***

Post Request Demo
    [Tags]      Demo
    Create Session    session1    ${base_url}       disable_warnings=1
    ${endpoint}       Set Variable      /api/users
    ${body}=          Create Dictionary     name=Rohit      job=SW Engineer
    ${response}=      POST On Session       session1    ${endpoint}     data=${body}
    
    #Validations
    ${status_code}=     Convert To String    ${response.status_code}    
    Should Be Equal    ${status_code}    201
    
    ${json_response}=   Convert String To Json    ${response.content}
    ${contents}=        Get Value From Json    ${json_response}    ${id_path}
    Should Not Be Empty    ${contents}


*** Comments ***
Test title
    [Tags]      Demo
    create session  session1    ${base_url}     disable_warnings=1
    ${endpoint}     set variable    /api/users?page=2
    ${response}=    get on session  session1    ${endpoint}

    #Validations
    ${status_code}=     convert to string   ${response.status_code}
    should be equal     ${status_code}  200

    ${json_response}=   convert string to json      ${response.content}
    ${contents}=        get value from json     ${json_response}    ${page_path}
    ${contents}=        convert to string       ${contents}
    ${contents}=        Remove String Using Regexp      ${contents}     ['\\[\\],]
    should be equal     ${contents}  2

    ${headerValue}=     get from dictionary     ${response.headers}     Content-Type
    should be equal     ${headerValue}  application/json; charset=utf-8
